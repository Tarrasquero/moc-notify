# moc-notify

Notificaciones para el reproductor moc, Linux.

[![IMAGE ALT TEXT HERE](https://img.youtube.com/vi/x6CiPKTay8o/0.jpg)](https://www.youtube.com/watch?v=x6CiPKTay8o)


[![IMAGE ALT TEXT HERE](https://img.youtube.com/vi/Pg2lRTem2O4/0.jpg)](https://www.youtube.com/watch?v=Pg2lRTem2O4)


## Uso en Debian y derivados:

- `sudo dpkg -i notify-moc-1.0.deb ; sudo apt-get install -f`
- Copiar el archivo `config` a ~/.moc o en su defecto modificar la linea: `OnSongChange = "/usr/bin/moc-notify %a %t %r %f"`
- Se recomienda encarecidamente usar `Dunst` como daemon de notificaciones.